Inter-process communication Module

    ipc = require 'ipc'

    $ = require 'jquery'

Get the elements by their id

    buttonNew    = $ '#button-new'
    divNew       = $ '#new'
    divStart     = $ '#start'
    butSaveNew   = $ '#save-new-file'

When the user clicks the "New" Button,

    buttonNew.on 'click', ->
      divStart.hide()
      divNew.show()
      ipc.send 'click-new'

When the user clicks the "Choose File" button

    butSaveNew.on 'click', ->
      pass = $('#new-pass').val()
      confirm = $('#new-pass-confirm').val()

      if pass is confirm
        ipc.on 'new-file-chose', (filename) ->

        ipc.send 'click-save-new'
      else
        $('#error').text "The passwords don't match"
