Module to control application life

    app = require 'app'

Inter-process communication

    ipc = require 'ipc'

Module to create native browser window

    BrowserWindow = require 'browser-window'

Keep a global reference to the window object to avoid it being garbage-collected

    mainWindow = null

Quit when all windows are closed

    app.on 'window-all-closed', () ->
      app.quit()

This method is called when Electron is ready to start creating windows

    app.on 'ready', ->
      mainWindow = new BrowserWindow width: 800, height: 600

Load main page from base64 url

      mainWindow.loadUrl "file://#{__dirname}/start.html"

Dereference the window object when the window is closed

      mainWindow.on 'closed', ->
        mainWindow = null

When the users clicks on the "New" button

    ipc.on 'click-new', ->
      console.log 'clicked-new from main'

    dialog = require 'dialog'
    ipc.on 'click-save-new', (event)->
      dialog.showSaveDialog mainWindow,
        title: 'New File',
        defaultPath: __dirname,
        (filename) ->
          event.sender.send 'new-file-chose', filename
